#include <QCoreApplication>
#include <QDebug>

#include "profileviewerlib.h"

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    ProfileViewerLib lib;
    qDebug() << lib.version();

    return a.exec();
}
