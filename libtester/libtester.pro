#-------------------------------------------------
#
# Project created by QtCreator 2014-10-16T12:25:11
#
#-------------------------------------------------

QT       += core

QT       -= gui

TARGET = libtester
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app


SOURCES += main.cpp

win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../ProfileViewerLib/release/ -lProfileViewerLib
else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../ProfileViewerLib/debug/ -lProfileViewerLib
else:unix: LIBS += -L$$OUT_PWD/../ProfileViewerLib/ -lProfileViewerLib

INCLUDEPATH += $$PWD/../ProfileViewerLib
DEPENDPATH += $$PWD/../ProfileViewerLib

win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../ProfileViewerLib/release/libProfileViewerLib.a
else:win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../ProfileViewerLib/debug/libProfileViewerLib.a
else:win32:!win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../ProfileViewerLib/release/ProfileViewerLib.lib
else:win32:!win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../ProfileViewerLib/debug/ProfileViewerLib.lib
else:unix: PRE_TARGETDEPS += $$OUT_PWD/../ProfileViewerLib/libProfileViewerLib.a
