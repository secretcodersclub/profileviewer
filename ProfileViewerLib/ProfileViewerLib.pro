#-------------------------------------------------
#
# Project created by QtCreator 2014-10-16T12:26:10
#
#-------------------------------------------------
QT       += core
QT       -= gui

TARGET = ProfileViewerLib
TEMPLATE = lib
CONFIG += staticlib

SOURCES += profileviewerlib.cpp

HEADERS += profileviewerlib.h
unix {
    target.path = /usr/lib
    INSTALLS += target
}
