#ifndef PROFILEVIEWERLIB_H
#define PROFILEVIEWERLIB_H

#include <QString>

class ProfileViewerLib
{

public:
    ProfileViewerLib();

    QString version() const;

private:
    QString m_version;
};

#endif // PROFILEVIEWERLIB_H
