#-------------------------------------------------
#
# Project created by QtCreator 2014-10-17T11:48:31
#
#-------------------------------------------------

QT       += core gui
qtHaveModule(printsupport): QT += printsupport

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Viewer
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    viewerarea.cpp \
    mygraphicsview.cpp \
    heatmap.cpp

HEADERS  += mainwindow.h \
    viewerarea.h \
    mygraphicsview.h \
    heatmap.h
