#ifndef HEATMAP_H
#define HEATMAP_H

#include <QColor>
#include <QGraphicsItem>
#include <vector>

//#include "uberfile.h"

class HeatMap : public QGraphicsItem
{
public:
    //HeatMap(int x, int y, const std::vector<size_t> & p, UberFile *uber);
    HeatMap(int x, int y, const std::vector<size_t> & p);

    QRectF boundingRect() const;
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *item, QWidget *widget);

protected:
    void mousePressEvent(QGraphicsSceneMouseEvent *event);
    void mouseMoveEvent(QGraphicsSceneMouseEvent *event);
    void mouseReleaseEvent(QGraphicsSceneMouseEvent *event);

private:
    int x;
    int y;
    //UberFile *uber;
    int rows;
    int cols;
    int h;
    int w;
    int cellW;
    int cellH;
    int rectWidth;
    int rectHeight;
    int medianCell;
    int numBins;
    QList<QColor> colors;
    QPainter *backPainter;
    QImage *image;
    QImage scaledImage;
};
#endif // HEATMAP_H
