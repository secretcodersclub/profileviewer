#include "heatmap.h"
#include <QtWidgets>
#include <QImage>
//#include "randomness.h"

//  Here is a segmentation uber that you would use to make a tree and heatmap.
//  /mnt/wigstore3/user/alexande/clickableHeat/uber.9727420.seg.quantal.5k.txt
//  Here's the matching normalized bincount data you would use for displaying genome profiles of single cells.
//  /mnt/wigstore3/user/alexande/clickableHeat/uber.9727420.lowratio.quantal.5k.txt
//  5000 bins by about 500 cells

//HeatMap::HeatMap(int x, int y, const std::vector<size_t> & p, UberFile *uber)
HeatMap::HeatMap(int x, int y, const std::vector<size_t> & p)
{
    int rX, rY;
    int binColorIndex;
    QColor c;
    int currChrom;
    double currMedian;

    this->cellW = 1;
    this->cellH = 1;


    // 880000 = median +2
    // FF0000 = median +1
    // FFFFFF = median
    // 0000FF = median -1
    // 000088 = median -2

    c.setNamedColor("#880000");
    colors << QColor(c);

    c.setNamedColor("#FF0000");
    colors << QColor(c);

    c.setNamedColor("#FFFFFF");
    colors << QColor(c);

    c.setNamedColor("#0000FF");
    colors << QColor(c);

    c.setNamedColor("#000088");
    colors << QColor(c);

    this->x = x;
    this->y = y;
    //this->uber = uber;

    setZValue((x + y) % 2);

    setFlags(ItemIsSelectable | ItemIsMovable);
    setAcceptHoverEvents(true);

    //this->w = ((int) uber->numCells)*this->cellW;
    //this->h = ((int) uber->numBins)*this->cellH + (24 * 2);

    this->image = new QImage(this->w, this->h, QImage::Format_ARGB32);
    this->backPainter = new QPainter(image);

    rX = 0;
    rY = 0;
//    //    for (int i = 0; i < uber->numCells; i++) {
//    for (std::vector<size_t>::const_iterator cell = p.begin(); cell != p.end(); ++cell) {

//        // round values to whole integers
//        // take median of a entire cell
//        // color code
//        currChrom=1;
//        //currMedian = this->uber->medianCell[i];
//        currMedian = this->uber->medianCell[*cell];
//        rY = 0;
//        for (int j = 0; j < this->uber->numBins; j++) {

//            if (currChrom!=this->uber->chrom[j]) {
//                backPainter->fillRect(0, rY, this->w, 4, QColor(Qt::black));
//                rY+=4;

//                currChrom = this->uber->chrom[j];
//            }

//            // double v = this->uber->binValues[i][j];
//            double v = this->uber->binValues[*cell][j];

//            // 880000 = median +2
//            // FF0000 = median +1
//            // FFFFFF = median
//            // 0000FF = median -1
//            // 000088 = median -2

//            if (v >= currMedian + 2.0) {
//                binColorIndex=0;
//            } else if ((v < currMedian + 2.0) && (v >= currMedian + 1.0)) {
//                binColorIndex=1;
//            } else if ((v < currMedian + 1.0) && (v >= currMedian)) {
//                binColorIndex=2;
//            } else if ((v < currMedian ) && (v >= currMedian - 1.0)) {
//                binColorIndex=3;
//            } else if (v < currMedian - 1.0 ) {
//                binColorIndex=4;
//            }

//            backPainter->fillRect(rX, rY, this->cellW, this->cellH, colors[binColorIndex]);
//            rY+=this->cellH;
//        }
//        rX+=this->cellW;
//    }

//    this->scaledImage = this->image->scaled(this->w, 800, Qt::IgnoreAspectRatio, Qt::SmoothTransformation);
//    this->h = 800;
}

QRectF HeatMap::boundingRect() const
{
    return QRectF(0, 0, w, h);
}

void HeatMap::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    Q_UNUSED(widget);
    Q_UNUSED(option);
    painter->drawImage(0,0,scaledImage,0,0);
}

void HeatMap::mousePressEvent(QGraphicsSceneMouseEvent *event)
{
    QGraphicsItem::mousePressEvent(event);
    update();
}

void HeatMap::mouseMoveEvent(QGraphicsSceneMouseEvent *event)
{
    if (event->modifiers() & Qt::ShiftModifier) {
        update();
        return;
    }
    QGraphicsItem::mouseMoveEvent(event);
}

void HeatMap::mouseReleaseEvent(QGraphicsSceneMouseEvent *event)
{
    QGraphicsItem::mouseReleaseEvent(event);
    update();
}
//EOF
